<?php
class AdminPage {

    // Actions attributes
    private $actionAvaliable = [
      'list',
      'create',
      'details',
      'delete',
    ];
    private $defaultAction = 'list';
    protected $admin_slug = 'admin_pages';
    protected $action = '';
    protected $customActions = false;
    // Schema storage
    protected $wpTable = true;
    protected $table = false; // To query non-wp table if $wpTable is true
    protected $tableFields = [];
    protected $fieldIDMap = false;
    protected $tableArgs = [];
    protected $posts_per_page = 20;
    protected $listTotal = 1;
    // Message render storage
    private $messageStatus = [
      'info',
      'success',
      'error',
      'warning',
    ];
    protected $messages = [];
    public $showMessages = true;

    public function __construct() {
      $this->initialize();
    }

    private function initialize():void {
      // Get & load action
      $action = empty($_GET['action']) ? '' : $_GET['action'];
      $this->action = !in_array($action, $this->actionAvaliable) ? $this->defaultAction : $action;

      // Initialize databse tables
      $this->setSchema();

      // $this->adminMenuSettings();
      $this->initAdmin();
    }

    private function addToAdminMenu(string $parent_slug, string $page_title, string $menu_title, string $capability, string $menu_slug, callable $function = null, int $position = null, string $icon_url = ''): void {
      add_action('admin_menu', function () use ($parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function, $position, $icon_url) {
        global $submenu;
        if (empty($submenu[$parent_slug])) add_menu_page($page_title, $menu_title, $capability, $parent_slug, $function, $icon_url, $position);
        else add_submenu_page($parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function, $position);
      });
    }

    // Ovverride to set schema using
    public function setSchema(): void {
    }

    public function setTable(string $table):bool { return ($this->table = $table); }
    public function getTable(): string { return $this->table; }

    public function getUrl(): string {
      return $_SERVER['REQUEST_URI'];
    }

    public function getBaseUrl(): string {
      return WP_HOME . '/wp-admin/admin.php?page=' . $this->admin_slug;
    }

    // Ovverride to set menu
    public function adminMenuSettings($menu = []):void {
      // $this->addToAdminMenu('admin_list', 'Admin List', 'Admin List', 'read', 'admin_list', array($this, 'render'));
      $this->addToAdminMenu($menu['slug'], $menu['title'], $menu['title'], 'read', $menu['slug'], array($this, 'render'));
    }

    private function actionRedirect():void {
      $this->redirect = 'admin.php?page=' . (empty($this->admin_slug) ? '' : $this->admin_slug);
      if (!empty($this->customRedirect)) $this->redirect = $this->customRedirect;
      if (!empty($this->redirect)) {
        wp_redirect($this->redirect);
        exit;
      }
    }

    public function render():void {
      // Always check table
      if (!isset($this->customGetList) && !$this->wpTable && $this->table === false) $this->addMessages(['message'=>'Warning: Missing table', 'status'=>'warning', ], true);

      if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $data = $_POST;
        
        $post = $this->handlePost($data);

        // Redirect
        if ($post) {
          if ($this->action == 'details') ;
          $this->actionRedirect();
        }
      }

      // Basic style
      echo '<style>
	.admin-details-actions { margin-top: 20px; }
        .admin-list-table-head { display: flex; margin: 10px 0; }
        .admin-list-table-head .wrapper-left { margin-right: auto; }
        .admin-list-table-head .wrapper-left h1, .admin-list-table-head .wrapper-left a { display: inline-block; }
        .admin-list-table-head .wrapper-left h1 { padding: 0; }
        .admin-list-table-head #search-post { margin-left: auto; }
      </style>';

      // Div at top
      echo '<div class="wrap-top">';
        echo $this->topSection();
      echo '</div>';

      // Handle views
      $action = $this->action;

      if (!empty($this->customActions)) {
        $default = $this->runCustomActions();
      }

      if (!isset($default) || !empty($default)) {
        switch($action) {
          case 'create':
          case 'details':
            $this->actionDetails();
            break;
          case 'delete':
            $this->actionDelete();
          case 'list':
          default:
            $this->actionList();
        }
      }

      // Div at bottom
      echo '<div class="wrap-bottom">';
        echo $this->bottomSection();
      echo '</div>';
    }

    /*
     *  Controller entries
     */

    protected function getParams($all = false):Array {
      if ($all) return $_GET;

      $params = [];
      if (isset($_GET['id'])) $params['id'] = intval($_GET['id']);
      if (isset($_GET['paged'])) $params['paged'] = intval($_GET['paged']);
      return $params;
    }

    public function actionList():void {
      $this->beforeFilter();

      $data = $this->getList();

      $data = $this->afterFilter($data);

      $this->beforeRender();

      $this->renderList($data);

      $this->afterRender();
    }

    private function getList(Array $args = []): Array {
      return $this->customGetList($args);
    }

    public function actionDetails():void {
      $this->beforeFilter();

      $params = $this->getParams();
      $data = $this->getDetails(empty($params['id']) ? 0 : $params['id']);

      $data = $this->afterFilter($data);

      $this->beforeRender();

      if ($this->action != 'create' && empty($data)) {
        $this->addMessages(['message'=>'Error: No data entry', 'status'=>'error', ], true);
        echo $this->renderNotices($this->messages);

        echo '<a class="button" onclick="javascript: window.history.go(-1); return false;">Back</a>';
      } else $this->renderDetails($data);

      $this->afterRender();
    }

    private function getDetails() {
      return $this->customGetDetails();
    }

    public function actionDelete():void {
      // Delete action
      $deleted = $this->handleDelete();
      if (!$deleted) {
        $this->addMessages(['message'=>'Error: Cannot delete entities. Please try again.', 'status'=>'error', ], true);
      }
      else {
        $this->addMessages(['message'=>'Success: Entity deleted successfully.', 'status'=>'success', ], true);
      }

      // Doing redirect
      if (false) // Disable redirect for alert messages
        $this->actionRedirect();
    }

    protected function addMessages(Array $msgs = [], bool $single = false):void {
      if ($single) $msgs = [$msgs];
      for ($i = 0; $i < sizeof($msgs); $i++) {
        $msg = $msgs[$i];
        $this->messages[] = [
          'message'=>empty($msg['message']) ? '' : $msg['message'],
          'status'=>empty($msg['status']) ? '' : $msg['status'],
          'id'=>empty($msg['id']) ? '' : $msg['id'],
          'dimissible'=>!empty($msg['dimissible']),
        ];
      }
    }

    /*
     *  End of Controller entries
     */

    /*
     *  Triggering callbacks
     */

    public function initAdmin() {}
    public function runCustomActions():bool {} // For override, to controll default actions return
    public function topSection() {}
    public function bottomSection() {}
    public function beforeFilter() {}
    public function afterFilter($data) { return $data; }
    public function beforeRender() {}
    public function afterRender() {}
    public function handlePost($data): bool {}
    public function handleDelete(): bool {}
    public $customRedirect = false;
    public function customGetList($args = []): Array { // For override
      if (empty($this->table)) $this->table = 'post';
      if (empty($this->tableFields)) $this->tableFields = ['ID'=>'ID'];

      $params = $this->getParams();
      $args = $this->tableArgs;
      if (empty($args)) $args = [
        'post_type'=>$this->table,
        'posts_per_page'=>$this->posts_per_page,
        'paged'=>empty($params['paged']) ? 1 : $params['paged'],
      ];
      if (!empty($_REQUEST['orderby'])) $args['orderby'] = $_REQUEST['orderby']; // sort
      if (!empty($_REQUEST['order'])) $args['order'] = $_REQUEST['order']; // order

      $query = new WP_Query(is_array($args) ? $args : []);

      $fields = array_keys($this->tableFields);
      array_walk($query->posts, function (&$v) use($fields) { $a = []; foreach($fields as $f) $a[$f] = $v->{$f}; $v = $a; });

      // Get total size
      $this->listTotal = $query->post_count;

      return $query->posts;
    }
    public function customListCreate($url): string { // For override
      return '<a href="' . $url . '&action=create" class="button">Add New</a>';
    }
    public function customSearchForm(): string { // For override
      // Handle custom data filtering at Listing()
      $table = empty($this->table) ? '' : $this->table; 
      $fields = empty($this->tableFields) ? ['ID'=>'ID'] : $this->tableFields;
      array_walk($fields, function (&$v, $k) { $v = "<option value='$k'>$v</option>"; });
      return '
        <form id="search-post" method="get">
          <div class="search-box">
	    <label class="screen-reader-text" for="post-search-input">Search ' . $table . ':</label>
            <input type="hidden" id="post-search-page" name="page" value="' . $this->admin_slug . '">
            <input type="hidden" id="post-search-action" name="action" value="search">
	    <select id="post-search-field" name="field">
              <option value="">Select searching field ...</option>
              ' . implode('', $fields) . '
            </select>
            <input type="search" id="post-search-input" name="query" value="">
            <input type="submit" id="search-submit" class="button" value="Search ' . $table . '">
          </div>
	</form>
      ';
    }
    public function customListActions($url, $data): string { // For override
      $colID = empty($this->fieldIDMap) ? 'ID' : $this->fieldIDMap;
      return '<td><a href="' . $url . '&action=details&id=' . $data[$colID] . '">Edit</a></td>';
    }
    public function customGetDetails() { // For override
      $params = $this->getParams();
      $args = $this->tableArgs;
      if (empty($args)) $args = [
        'post_type'=>$this->table,
        'p'=>$params['id']
      ];

      $data = new WP_Query(is_array($args) ? $args : []);
      return empty($data->posts) ? false : $data->posts[0];
    }

    /*
     *  View entries
     */

    protected function renderNotices(Array $msgs = []):string {
      $output = '';
      for($i =0; $i < sizeof($msgs); $i++) {
        $msg = $msgs[$i];
        $id = empty($msg['id']) ? '' : $msg['id'];
        $str = empty($msg['message']) ? '' : $msg['message'];
        $status = empty($msg['status']) ? '' : $msg['status'];
        $output .= $this->renderNotice($str, $status, $id, !empty($msg['dismissible']));
      }
      return $output;
    }

    private function renderNotice(string $message = '', string $status = 'success', $id = false, bool $dismissible = true):string {
      return "
        <div" . ($id ? " id='$id'" : '') . " class='notice notice-" . (in_array($status, $this->messageStatus) ? $status : 'success') . ($dismissible ? ' is-dismissible' : '') . "'> 
          <p><strong>" . (empty($message) ? '&nbsp;' : $message) . "</strong></p>
          <button type='button' class='notice-dismiss'><span class='screen-reader-text'>Dismiss this notice.</span></button>
        </div>
      ";
    }

    /*
     *  For overrideed
     */
    public function renderList($data = []):void {
      if ($this->showMessages) echo $this->renderNotices($this->messages);

      $table = $this->table;
      $fields = empty($this->fieldIDMap) ? array_merge(['ID'=>'ID'], $this->tableFields) : $this->tableFields;
      $colID = empty($this->fieldIDMap) ? 'ID' : $this->fieldIDMap;
      $url = $this->getUrl();
      $baseUrl = $this->getBaseUrl();
      $params = $this->getParams();
      $pages = ceil($this->listTotal / $this->posts_per_page);
      $paged = empty($params['paged']) ? 1 : $params['paged'];
      $isFirstPage = $paged == 1;
      $isLastPage = $paged == $pages || $paged == $pages;

      echo '<div class="wrap">';
        echo '<h1 class="wp-heading-inline"></h1>'; // For layout issue
        echo '<div class="admin-list-table-head">';
          echo '<div class="wrapper-left">';
            echo '<h1 class="">' . ucfirst($table) . '</h1> ' . $this->customListCreate($baseUrl);
          echo '</div>';
	  echo $this->customSearchForm();
        echo '</div>';
        echo '<form id="posts-filter" method="post" action="' . $url . '&action=batch">';
          echo '<table class="wp-list-table widefat fixed striped table-view-list posts">';
            echo '<thead>';
              echo '<tr>';
                foreach ($fields as $field) {
                  $order = !empty($_REQUEST['order']) && $_REQUEST['order'] == strtolower($field) && !empty($_REQUEST['orderby']) ? ($_REQUEST['order'] == 'asc' ? 'desc' : 'asc') : '';
                  if ($field == $colID) echo '<td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1">Select All</label><input id="cb-select-all-1" type="checkbox"></td>';
                  else echo '<th scope="col" id="' . strtolower($field) . '" class="manage-column column-' . strtolower($field) . '"><a href="' . $baseUrl . '&order=' . strtolower($field) . '&orderby=' . (empty($order) ? 'asc' : $order) . '">' . ucfirst($field) . ' ' . (empty($order) ? '' : ($order == 'asc' ? '▼' : '▲')) . '</a></th>';
                }
                echo '<th scope="col" id="actions" class="manage-column column-actions">Actions</th>';
              echo '</tr>';
            echo '</thead>';
            echo '<tbody id="the-list">';
              if (empty($data))
              echo '<tr><td colspan="' . (sizeof($fields)+1) . '">Empty row.</td></tr>';
              else {
                foreach ($data as $row) {
              echo '<tr id="data-' . $row[$colID] . '" class="iedit author-self level-0 data-' . $row[$colID] . '">';
                  foreach($row as $key=>$value) {
                    if ($key == $colID) {
                echo '<th scope="row" class="check-column">';
                  echo '<label class="screen-reader-text" for="cb-select-' . $row[$key] . '">Select ' . $table . '</label>';
                  echo '<input id="cb-select-' . $row[$key] . '" type="checkbox" name="post[]" value="' . $row[$key] . '">';
                echo '</th>';
                    }
                    else {
                echo '<td class="' . $row[$key] . ' column-' . $row[$key] . '" data-colname="' . ucfirst($row[$key]) . '">' . $row[$key] . '</td>';
                    }
                  }
                echo $this->customListActions($baseUrl, $row);
              echo '</tr>';
                }
              }
            echo '</tbody>';
          echo '</table>';
          echo '<div class="tablenav bottom">';
            echo '<div class="tablenav-pages">';
              echo '<span class="displaying-num">' . $this->listTotal . ' items</span>';
              // previous pages
              echo !$isFirstPage ? '<a class="next-page button" href="' . $baseUrl . '&paged=1">' : '<span class="tablenav-pages-navspan button disabled" aria-hidden="true">';
                echo '«';
              echo !$isFirstPage ? '</a>' : '</span>';
              echo !$isFirstPage ? '<a class="next-page button" href="' . $baseUrl . '&paged=' . ($paged-1) . '">' : '<span class="tablenav-pages-navspan button disabled" aria-hidden="true">';
                echo '‹';
              echo !$isFirstPage ? '</a>' : '</span>';
              // page info
              echo '<span class="screen-reader-text">Current Page</span><span id="table-paging" class="paging-input"><span class="tablenav-paging-text">' . $paged . ' of <span class="total-pages">' . $pages . '</span></span></span>';
              // next pages
              echo !$isLastPage ? '<a class="next-page button" href="' . $baseUrl . '&paged=' . ($paged+1) . '">' : '<span class="tablenav-pages-navspan button disabled" aria-hidden="true">';
                echo '<span class="screen-reader-text">Next page</span><span aria-hidden="true">›</span>';
              echo !$isLastPage ? '</a>' : '</span>';
              echo !$isLastPage ? '<a class="last-page button" href="' . $baseUrl . '&paged=' . $pages . '">' : '<span class="tablenav-pages-navspan button disabled" aria-hidden="true">';
                echo '<span class="screen-reader-text">Last page</span><span aria-hidden="true">»</span>';
              echo !$isLastPage ? '</a>' : '</span>';
            echo '</div>';
          echo '</div>';
        echo '</form>';
      echo '</div>';
    }

    /*
     *  For overrideed
     */
    public function renderDetails($data = []):void {
      if ($this->showMessages) echo $this->renderNotices($this->messages);

      echo '<form method="post">';
        echo '<h1 class="wp-heading-inline">' . (empty($this->table) ? 'Post' : $this->table) . '</h1>';
        echo '<div class="edit-details-actions">';
          echo '<input type="submit" class="button button-primary" value="Submit" />';
          echo '<a href="javascript:history.back();" class="button">Cancel</a>';
        echo '</div>';
      echo '</form>';
    }

    /*
     *  End of View entries
     */
}
